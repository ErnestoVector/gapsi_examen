package com.egmdevelopers.gapsi_examen.ui.adapters

import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.egmdevelopers.gapsi_examen.R


/**
 * BindingAdapters
 * @author: Ernesto Gálvez Martínez
 */
@BindingAdapter("imageUrl")
fun ImageView.bindImage(imageUrl: String?) {
    imageUrl?.let {
        val uri = imageUrl.toUri().buildUpon().scheme("https").build()
        val options = RequestOptions()
            .placeholder(R.drawable.loading_animation)
            .error(R.drawable.ic_broken_image)

        Glide.with(this.context)
            .load(uri)
            .apply(options)
            .into(this)
    }
}
