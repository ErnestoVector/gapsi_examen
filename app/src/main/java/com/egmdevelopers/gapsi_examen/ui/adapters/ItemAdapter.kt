package com.egmdevelopers.gapsi_examen.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.egmdevelopers.gapsi_examen.api.models.ItemResponse
import com.egmdevelopers.gapsi_examen.databinding.ItemPrototypeBinding


/**
 * ItemAdapter
 * @author: Ernesto Gálvez Martínez
 */
class ItemAdapter : ListAdapter<ItemResponse, ItemAdapter.ItemViewHolder>(DiffCallback) {

    // =============================================================================================
    //     RECYCLER VIEW
    // =============================================================================================
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder
        = ItemViewHolder(ItemPrototypeBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val property = getItem(position)
        holder.apply {
            bind(property)
        }
    }


    // =============================================================================================
    //     VIEW HOLDER
    // =============================================================================================
    class ItemViewHolder(private var binding: ItemPrototypeBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ItemResponse) = with(binding) {
            property = item
            price = "$${item.price}"
            executePendingBindings()
        }

    }


    // =============================================================================================
    //     COMPANION OBJECT
    // =============================================================================================
    companion object DiffCallback : DiffUtil.ItemCallback<ItemResponse>() {
        override fun areItemsTheSame(oldItem: ItemResponse, newItem: ItemResponse): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ItemResponse, newItem: ItemResponse): Boolean {
            return oldItem.id == newItem.id
        }
    }

}