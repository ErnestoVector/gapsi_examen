package com.egmdevelopers.gapsi_examen.ui.firstfragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.egmdevelopers.gapsi_examen.api.ApiService
import com.egmdevelopers.gapsi_examen.api.ApiStatus
import com.egmdevelopers.gapsi_examen.api.models.SearchResponse
import kotlinx.coroutines.launch


/**
 * FirstFragmentViewModel
 * @author: Ernesto Gálvez Martínez
 */
class FirstFragmentViewModel : ViewModel() {

    // =============================================================================================
    //     PROPERTIES
    // =============================================================================================
    // Mutable Live Data
    private val _status = MutableLiveData<ApiStatus>()
    private val _response = MutableLiveData<SearchResponse>()

    // Live Data
    val status : LiveData<ApiStatus>
        get() = _status
    val response : LiveData<SearchResponse>
        get() = _response

    // =============================================================================================
    //     INIT
    // =============================================================================================
    init {
        getApiData("")
    }


    // =============================================================================================
    //     UI
    // =============================================================================================
    fun getApiData(searchFor: String) {
        viewModelScope.launch {
            _status.value = ApiStatus.LOADING
            try {
                Log.d(TAG, "SUCCESS")
                _response.value = ApiService.retrofitService.getSearchFor(searchFor)
                _status.value   = ApiStatus.DONE
            } catch (e: Exception) {
                Log.d(TAG, "ERROR: ${e.message}")
                _response.value = null
                _status.value   = ApiStatus.ERROR
            }
        }
    }


    // =============================================================================================
    //     COMPANION OBJECT
    // =============================================================================================
    companion object {
        private val TAG = FirstFragmentViewModel::class.java.simpleName
    }

}