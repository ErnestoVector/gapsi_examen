package com.egmdevelopers.gapsi_examen.ui.firstfragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.egmdevelopers.gapsi_examen.R
import com.egmdevelopers.gapsi_examen.api.ApiStatus
import com.egmdevelopers.gapsi_examen.api.models.SearchResponse
import com.egmdevelopers.gapsi_examen.databinding.FragmentFirstBinding
import com.egmdevelopers.gapsi_examen.ui.adapters.ItemAdapter

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(), View.OnClickListener {

    // =============================================================================================
    //     PROPERTIES
    // =============================================================================================
    private val vm : FirstFragmentViewModel by lazy {
        ViewModelProvider(this).get(FirstFragmentViewModel::class.java)
    }
    private lateinit var binding: FragmentFirstBinding

    private val itemAdapter = ItemAdapter()


    // =============================================================================================
    //     FRAGMENT LIFECYCLE
    // =============================================================================================
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirstBinding.inflate(inflater)

        binding.apply {
            viewModel           = vm
            rvResult.adapter    = itemAdapter
        }
        initObservers()
        initClickListeners()

        return binding.root
    }

    // =============================================================================================
    //     INIT
    // =============================================================================================
    private fun initObservers() {
        vm.response.observe(viewLifecycleOwner, responseObserver)
        vm.status.observe(viewLifecycleOwner, statusObserver)
    }

    private fun initClickListeners() {
        binding.apply {
            btnSearch.setOnClickListener(this@FirstFragment)
        }
    }

    // =============================================================================================
    //     ON CLICK
    // =============================================================================================
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSearch -> searchForText()
        }
    }

    private fun searchForText() {
        val searchFor = binding.etInput.text.toString()
        vm.getApiData(searchFor)

        // TODO: Save this search
    }


    // =============================================================================================
    //     OBSERVERS
    // =============================================================================================
    private val responseObserver = Observer<SearchResponse> {
        it?.let {
            itemAdapter.submitList(it.items)
        }
    }

    private val statusObserver = Observer<ApiStatus> {
        it?.let{
            binding.ivLoading.apply {
                when (it) {
                    ApiStatus.DONE -> visibility = View.GONE
                    ApiStatus.LOADING -> {
                        visibility = View.VISIBLE
                        setImageResource(R.drawable.loading_animation)
                    }
                    ApiStatus.ERROR -> {
                        visibility = View.VISIBLE
                        setImageResource(R.drawable.ic_connection_error)
                    }
                }
            }
        }
    }

}