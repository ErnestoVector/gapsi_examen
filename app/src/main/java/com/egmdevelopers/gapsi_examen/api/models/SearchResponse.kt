package com.egmdevelopers.gapsi_examen.api.models

/**
 * SearchResponse
 * @author: Ernesto Gálvez Martínez
 */
data class SearchResponse(
    val totalResults    : Int?,
    val page            : Int?,
    val items           : List<ItemResponse>?
)
