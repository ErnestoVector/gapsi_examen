package com.egmdevelopers.gapsi_examen.api.models

/**
 * ItemResponse
 * @author: Ernesto Gálvez Martínez
 */
data class ItemResponse(
    val id      : String?,
    val rating  : Double?,
    val price   : Double?,
    val image   : String?,
    val title   : String?
)
