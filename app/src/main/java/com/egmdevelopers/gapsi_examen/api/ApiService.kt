package com.egmdevelopers.gapsi_examen.api

object ApiService {

    val retrofitService: EndPoints by lazy {
        retrofit.create(EndPoints::class.java)
    }

}