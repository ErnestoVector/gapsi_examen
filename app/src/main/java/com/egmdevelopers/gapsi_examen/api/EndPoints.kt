package com.egmdevelopers.gapsi_examen.api

import com.egmdevelopers.gapsi_examen.api.models.SearchResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


/**
 * EndPoints
 * @author: Ernesto Gálvez Martínez
 */
interface EndPoints {

    @Headers("$HEADER_TITLE: $HEADER_VALUE")
    @GET("search")
    suspend fun getSearchFor(@Query("query") searchObject: String) : SearchResponse


}