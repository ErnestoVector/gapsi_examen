package com.egmdevelopers.gapsi_examen.api

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


/**
 * UrlConstants
 * @author: Ernesto Gálvez Martínez
 */
// Main URL
private const val BASE_URL = "https://00672285.us-south.apigw.appdomain.cloud/demo-gapsi/"

// Headers
const val HEADER_TITLE  = "X-IBM-Client-Id"
const val HEADER_VALUE  = "adb8204d-d574-4394-8c1a-53226a40876e"

// Moshi builder
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

// Api Builder
internal val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()