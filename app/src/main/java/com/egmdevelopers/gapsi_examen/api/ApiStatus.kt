package com.egmdevelopers.gapsi_examen.api


/**
 * ApiStatus
 * @author: Ernesto Gálvez Martínez
 */
enum class ApiStatus {
    LOADING, ERROR, DONE
}